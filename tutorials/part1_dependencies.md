#Part I: Install dependencies
###git

For Debian/Ubuntu

	sudo apt-get install git
	
Instruction for mac and other operating systems can be found at http://git-scm.com/book/en/v2/Getting-Started-Installing-Git. The default download for mac only works for OS X 10.9 and above. For older versions of OS X, download the corresponding git package from http://sourceforge.net/projects/git-osx-installer/files/ and click through the installer. Check to make sure that you have git by typing

	which git
	git

It should give you the pathway of the git executable, and a list of git commands.

cd into your home directory. Clone this git repository:
	
	cd ~
	git clone https://yangya@bitbucket.org/yangya/phylogenomic_dataset_construction.git
	
A directory named phylogenomic_dataset_construction should be copied to your home directory. 

###TransDecoder
Create a directory called "apps" in the home directory and download apps by cloning git repositories. Install TransDecoder.

	mkdir apps
	cd apps
	git clone https://github.com/TransDecoder/TransDecoder.git
	cd ~/apps/TransDecoder
	make
	
###python

Check the version of your python

	python
	
Code is tested in python 2.7.x and is compatible with python 2.6. However, it won't work with 3.x.

###blast+ package
Download the excutables from ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/LATEST/. Choose the x64-linux.tar.gz if you are using linux, and the universal-macosx.tar.gz file for mac (not the .dmg one). Unzip by click or right-click the downloaded file. This will create a new directory named ncbi-blast-2.2.30+. Move this directory to ~/apps.

Append the path by open the file ~/.bashrc in your favorite text editor. For mac TextWrangler would work (Word or TextEdit do not). For Linux geany, gedit, vi etc would all work. Since .bashrc is a hidden file, make sure that your text editor shows hidden file if you try to open it from GUI. Paste in this line at the end of the file and save it:

	export PATH=~/apps/ncbi-blast-2.2.30+/bin:$PATH

Type

	source ~/.bashrc

to update the bash profile. Now all the blast excutables should be in the PATH. Type

	makeblastdb

It should print out a list of command line arguments. It works for linux but on mac you might need to edit and source the file ~/.bash_profile instead of ~/.bashrc. Alternatively, if editing the bash profile did not work you can also copy the executables we will be using to the PATH by typing
	
	sudo cp ~/apps/ncbi-blast-2.2.30+/bin/makeblastdb /usr/local/bin
	sudo cp ~/apps/ncbi-blast-2.2.30+/bin/blast? /usr/local/bin

###mcl
Download mcl-latest.tar.gz from http://micans.org/mcl. Unzip the downloaded file and move the directory mcl-14-137 to ~/apps. Install the executable to your path. If /usr/local/bin is not in your path you can use a different installation prefix, for example, /usr. 

	cd ~/apps/mcl-14-137
	./configure --prefix=/usr/local
	sudo make install

Type
	
	mcl

to make sure that it is in the path

###phyutility
Download phyutility from https://code.google.com/p/phyutility/downloads/list. Unzip, and move the directory to ~/apps.

Put phyutility in the path. First, get the absolute path for the phyutility.jar file. For example, mine on the mac is /Users/yangya/apps/phyutility/phyutility.jar. Next, make a new text file named "phyutility" in your path (I put it in /usr/local/bin. Use echo $PATH to see your path), and write the following line in it pointing to the absolute path of the jar file. Use the actual path on your system of course:

	java -Xmx6g -jar <path to phyutility.jar> $*

Make the phyutility.jar file executable

	chmod u+x <path to phyutility.jar>

Type 

	phyutility

to make sure that it is correctly installed. It should print out a list of commands. 

###mafft
Download mafft from http://mafft.cbrc.jp/alignment/software/ and follow the instructions. For mac, use the standard package from http://mafft.cbrc.jp/alignment/software/macosx.html and click through the installation. Again, type 

	which mafft
	mafft

to make sure that the executable named mafft is correctly installed in the path

###raxml
Clone the raxml git repository and follow the instruction in the README file for compilation instructions. 

	cd ~/apps
	git clone https://github.com/stamatak/standard-RAxML.git
	
After compilation, you will get an executable file. Mine is named raxmlHPC-PTHREADS-SSE3. Yours could be slightly different. Make sure that it is executable by typing
	
	chmod u+x <path to raxml executable>
	
Copy the executable to your path as a file named "raxml"

	sudo cp <path to raxml executable> /usr/local/bin/raxml

Again, type

	raxml -h

to see the list of commands.

You will also need programs for visualizing alignments and trees. Use any of your favorite ones. Jalview http://www.jalview.org/ is a nice one for visualizing very large alignment (we won't encounter large alignment for the tutorial though). Figtree is good for visualizing trees. 

For the tutorial we won't generate very large alignments or trees. However, for real data sets it's nice to have programs such as FastTree and PASTA to handle homologs with more than 1000 sequences.
