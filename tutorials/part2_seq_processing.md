
#Part II: Sequence processing, assembly and translation
cd in to the directory examples/seqcore_pe_stranded.

	cd ~/phylogenomic_dataset_construction/examples/seqcore_pe_stranded
	
The two .gz files are read pairs from a stranded transcriptome sequence of Drosera binata, and is about 1/20 of a full data set. You can see the first 10 lines of the file on linux by typing

	zcat 41624_CTTGTA_L005_R1_010.fastq.gz | head

To count how many lines there are in the file, type

	zcat 41624_CTTGTA_L005_R1_010.fastq.gz | wc -l
	
Divide the line numbers by 4 you can get the number of reads. On mac use gunzip -c instead of zcat:

	gunzip -c 41624_CTTGTA_L005_R1_010.fastq.gz | head
	gunzip -c 41624_CTTGTA_L005_R1_010.fastq.gz | wc -l

Choose a taxonID for each data set. This taxonID will be used throughout the analysis. Use short taxonIDs with 4-6 letters and digits with no special characters. Here we use Dbin as the taxonID, which is short for Drosera binata.

##*De novo* assembly with Trinity
A typical Trinity call for analyzing Smith Lab sequences looks like (custom adapter file, use trimmomatic for both adapter clipping and quality trimming, stranded):

	ulimit -s unlimited
	Trinity --seqType fq --trimmomatic --quality_trimming_params "ILLUMINACLIP:/home/yayang/optimize_assembler/data/TruSeq_adapters:2:30:10 SLIDINGWINDOW:4:5 LEADING:5 TRAILING:5 MINLEN:25" --max_memory 100G --CPU 9 --full_cleanup --SS_lib_type RF --output taxonID.trinity --left <forward reads> --right <reverse reads>

I use filter\_fastq.py for sequences downloaded from SRA when adapter sequences and phred score offset unknown. For data sets with known adapter and phred scores I use Trimmomatic instead. Trimmomatic is much faster but would not give an error message if you the adapter or phred score offset were misspedified.

For tutorial we don't run Trinity since installing Trinity can be tricky and you don't use a laptop to run Trinity anyways. A Trinity output file Dbin.Trinity.fasta is provided for the data set seqcore_pe_stranded.
	
##Step 3: Find open reading frames and translate using TransDecoder with blastp for choosing orfs

TransDecoder provides two options for choosing the best orfs. I benchmarked using blastp vs. PfamAB and found that since we have closely related, high quality proteomes available for the Caryophyllales, blastp using a custom blast database is faster and more sensitive than Pfam. 

Create a custem blast database using closely-related, high quality proteomes:

	cd ~/phylogenomic_dataset_construction/data/blastp_database
	cat *fa >db
	makeblastdb -in ./db -parse_seqids -dbtype prot -out db

cd into the directory where the Trinity output file is

	cd ~/phylogenomic_dataset_construction/examples/seq_processing/seqcore_pe_stranded
	
For Smith Lab data sets (-S for stranded. Remove -S for non-stranded data sets):

	~/apps/TransDecoder/TransDecoder.LongOrfs -t Dbin.Trinity.fasta -S
	blastp -query Dbin.Trinity.fasta.transdecoder_dir/longest_orfs.pep -db ~/phylogenomic_dataset_construction/data/blastp_database/db -max_target_seqs 1 -outfmt 6 -evalue 10 -num_threads 2 > taxonID.blastp.outfmt6
	~/apps/TransDecoder/TransDecoder.Predict -t Dbin.Trinity.fasta --retain_blastp_hits taxonID.blastp.outfmt6 --cpu 2
	
The blastp step takes about 10 minutes to finish with two cores. In the mean time you can open the unfinished result file taxonID.blastp.outfmt6. The top blast hit is usually Beta vulgaris, the species more closely-related to Drosera. Occasionally the top hit is Arabidopsis thaliana, which is slightly more distantly related but better annotated. The blastp step is only for picking ORF so as long as an ORF hits something it will be retained in the final results. 

The TransDecoder output files are taxonID.Trinity.fasta.transdecoder.pep and taxonID.Trinity.fasta.transdecoder.cds. Shorten the namess to taxonID@seqID. The special character "@" is used to separate taxonID and seqID.

	python ~/phylogenomic_dataset_construction/fix_names_from_transdecoder.py . .
	
You can modify the script fix_names_from_transdecoder.py if you want to instead retain the subcomponent and isoform information output by Trinity.