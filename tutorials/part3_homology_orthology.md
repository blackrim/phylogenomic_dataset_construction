
##Step 4: Clustering

We provided a small data set of five taxa in the directory ~/phylogenomic\_dataset\_construction/examples/homology\_inference. The input can be cds or peptides. Because these five species are fairly closely-related (< 50 my), we use cds for homology inference. First, check for duplicated names, special characters other than digits, letters and "_", all names follow the format taxonID@seqID, and file names are the taxonID. It's good to check especially when some of the data sets were obtained from elsewhere. Most peptides and CDS files from genome annotation contain long names, spaces and special characters and should be eliminated before clustering.

	python ~/phylogenomic_dataset_construction/check_names.py data .fa

Reduce redundancy for cds (use -r 0 since these should all be positive strand after translation):
	
	cd ~/phylogenomic_dataset_construction/examples/homology_inference/data
	~/apps/TransDecoder/util/bin/cd-hit-est -i Beta.cds.fa -o Beta.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i HURS.cds.fa -o HURS.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i NXTS.cds.fa -o NXTS.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i RNBN.cds.fa -o RNBN.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2
	~/apps/TransDecoder/util/bin/cd-hit-est -i SCAO.cds.fa -o SCAO.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T 2

Concatenate all the .cdhitest files into a new file all.fa

	mkdir ../1_clustering
	cat *.cdhitest >../1_clustering/all.fa
	
All-by-all blast

	cd ../1_clustering
	makeblastdb -in all.fa -parse_seqids -dbtype nucl -out all.fa
	blastn -db all.fa -query all.fa -evalue 10 -num_threads 2 -max_target_seqs 30 -out all.rawblast -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'

This will take a few minutes. Filter raw blast output by hit fraction and prepare input file for mcl. I usually use 0.3 or 0.4 for hit\_fraction_cutoff when using sequences assembled from RNA-seq depending on how divergent the sequences are. A low hit-fraction cutoff will output clusters with more incomplete sequences and larger and sparser alignments, whereas a high hit-fraction cutoff gives tighter clusters but ignores incomplete or divergent sequences.

	python ~/phylogenomic_dataset_construction/blast_to_mcl.py all.rawblast 0.4

As it is running , it outputs between taxa blast hits that are nearly identical. These can be contamination, but can also be bacteria sequences. Double check when two samples have an unusually high number of identical sequences and this can be a sign of contamination. 

Run mcl. "--te" specifies number of threads, "-I" specifies the inflation value, and -tf 'gq()' specifies minimal -log transformed evalue to consider, and "-abc" specifies the input file format.

	mcl all.rawblast.hit-frac0.4.minusLogEvalue --abc -te 2 -tf 'gq(5)' -I 1.4 -o hit-frac0.4_I1.4_e5

Write fasta files for each cluster from mcl output that have all 5 taxa. Make a new directory to put the thousands of output fasta files.
	
	mkdir ../2_clusters
	python ~/phylogenomic_dataset_construction/write_fasta_files_from_mcl.py all.fa hit-frac0.4_I1.4_e5 5 ../2_clusters

Now we have a new directory with fasta files that look like cluster1.fa, cluster2.fa and so on. For tutorial purpose let's only use 1% of the clusters

	mkdir ../2_clusters_00
	cp ../2_clusters/*00 ../2_clusters_00

	
##Build homolog trees

Align each cluster, trim alignment, and infer a tree. For clusters that have less than 1000 sequences, it will be aligned with mafft (--genafpair --maxiterate 1000), trimmed by a minimal column occupancy of 0.1 and tree inference using raxml. For larger clusters it will be aligned with pasta, trimmed by a minimal column occupancy of 0.01 and tree inference using fasttree. The ouput tree files look like clusterID.raxml.tre or clusterID.fasttree.tre for clusters with 1000 or more sequences. 

	python fasta_to_tree.py <fasta dir> <number_cores> dna/aa bootstrap(y/n)

Trim tips that are longer than a relative length cutoff and more than 10 times longer than its sister. Also trim tips that are longer than an absolute value. The output tree ends with ".tt". Keep input and output trees in the same directory.

	python trim_tips.py <input tree dir> <tree_file_ending> <relative_cutoff> <absolute_cutoff>

Mask both mono- and (optional) paraphyletic tips that belong to the same taxon. Keep the tip that has the most un-ambiguous charactors in the trimmed alignment. Keep input and output trees in the same directory.

	python mask_tips_by_taxonID_transcripts.py <.tt dir> <aln-cln dir> mask_paraphyletic(y/n)

For phylogenomic data sets that are from annotated genomes, I would only mask monophyletic tips, and keep the sequence with the shortest terminal branch length. Keep input and output trees in the same directory.

	python mask_tips_by_taxonID_genomes.py <.tt dir>

Cut deep paralogs. If interested in building phylogeny a lower (more stringent) long_internal_branch_cutoff should be used. Use a higher (more relaxed) cutoff if interested in homologs to avoid splitting homologs. This works very well with CDS and less effective amino acid squences. For CDS the branch lengths are mostly determined by synonymous distance and are more consistant than for amino acids. Make sure that the indir and outdir are different directories.

	python cut_long_internal_branches.py <input tree dir> <input tree file ending> <internal_branch_length_cutoff> <minimal no. taxa> <outDIR>

Write fasta files from trees. The imput tree file ending should be .subtree

	python write_fasta_files_from_trees.py all.fa <cut tree dir> <tree_file_ending> <outDIR>

Repeat the alignment tree estimation, trimming, masking and cutting deep paralogs. Can use a set of more stringent cutoffs in the second round. After the final round, write fasta files from trees using tree files that ends with both .subtree and .tt.mm, and estimate the final homolog trees.

Alternatively one can calculate the synonymous distance and use that to guide cutting. However, since we are only trying to get well-aligned clusters for tree inference, choice of length cutoffs here can be somewhat arbitary. 

From here a number of further analyses can be done with the homologs, such as gene tree discordance and back translate peptide alignment to codons with pal2nal and investigate signature of natural selection.


##Step 6: Paralogy pruning to infer orthologs. Use one of the following:

1to1: only look at homologs that are strictly one-to-one. No cutting is carried out.
	
	python filter_1to1_orthologs.py <homologDIR> <tree_file_ending> <minimal_taxa> <outDIR>

MI: prune by maximum inclusion. The long_tip_cutoff here is typically the same as the value used when trimming tips. Set OUTPUT_1to1_ORTHOLOGS to False if wish only to ouput orthologs that is not 1-to-1, for example, when 1-to-1 orthologs have already been analyzed in previous steps.

	python prune_paralogs_MI.py <homologDIR> <tree_file_ending> <relative_long_tip_cutoff>  <absolute_long_tip_cutoff> <minimal_taxa> <outDIR>

MO: prune by using homologs with monophyletic, non-repeating outgroups, reroot and cut paralog from root to tip. If no outgroup, only use those that do not have duplicated taxa. Change the list of ingroup and outgroup names first. Set OUTPUT_1to1_ORTHOLOGS to False if wish only to ouput orthologs that is not 1-to-1

	python prune_paralogs_MO.py <homologDIR> <tree_file_ending> <minimal_taxa> <outDIR>

RT: prune by extracting ingroup clades and then cut paralogs from root to tip. If no outgroup, only use those that do not have duplicated taxa. Compile a list of ingroup and outgroup taxonID, with each line begin with either "IN" or "OUT", followed by a tab, and then the taxonID.

	python prune_paralogs_RT.py <homologDIR> <tree_file_ending> <outDIR> <minimal_ingroup_taxa> <ingroup and outgroup taxonIDs>

Or alternatively, if the input homolog tree is already rooted:

	python prune_paralogs_from_rooted_trees.py <homoTreeDIR> <tree_file_ending> <minimal_taxa> <outDIR>


##Step 7: Visualize matrix occupancy stats and constructing the supermatrix

	python ortholog_occupancy_stats.py <ortho_treDIR>

Reaqd in and rank number of taxa per ortholog from highest to lowest. Plot the ranked number of taxa per ortholog

	a = read.table('ortho_stats')
	downo = order(a[,1],decreasing=T)
	pdf(file="taxon_occupancy.pdf")
	plot(a[downo,1])
	dev.off()

Check taxon_stats to see if any taxa have unusally low number of genes in the orthologs. Open the file taxon_occupancy.pdf and decide the MIN_TAXA filter. Write new fasta files from ortholog trees

	python write_ortholog_fasta_files.py <fasta file with all seqs> <ortholog tree DIR> outDIR MIN_TAXA

Align final orthologs. Play with a few alignment methods here. Try prank or fsa in addition to mafft or sate for more accurate alignments. Prank tend to create lots of gaps when it is not sure about the homology so make sure to check the alignment visually.

	python prank_wrapper.py <inDIR> <outDIR> <file ending> DNA/aa

Trim alignment. I usually use 0.3 for MIN_COLUMN_OCCUPANCY

	python phyutility_wrapper.py <inDIR> <MIN_COLUMN_OCCUPANCY> DNA/aa

Or use Gblocks for trimming alignments if the sequences are very divergent (change FILE_ENDING first):

	python pep_gblocks_wrapper.py <inDIR> <outDIR>

Choose the minimal cleaned alignment length and minimal number of taxa filters for whether to include an ortholog in the supermatrix. Concatenate selected cleaned matrices:

	python concatenate_matrices.py <aln-clnDIR> <numofsites> <numoftaxa> DNA/aa <outfile>

This will output a list of cleaned orthology alignments that passed the filter, a summary of taxon matrix occupancies to check whether any taxon is under represented, and a concatenated matrix in phylip format


##Step 8: Estimate species tree

Run raxml with each ortholog as a separate partition. Use GTRCAT instead of PROTCATWAG for dna

	raxml -T <num_cores> -p 12345 -m PROTCATWAG -q <.model> -s <.phy> -n <output>

Run raxml with 200 rapid bootstrap replicates and search for the best tree. Use GTRCAT instead of PROTCATWAG for dna

	raxml -T 9 -f a -x 12345 -# 200 -p 12345 -m PROTCATWAG -q <.models file> -s <.phy file> -n <output>

Use examl if the matrix is large:
	
	python examl_wrapper.py <.phy file> <.model file> <outname> <number_cores> DNA/aa

Run 200 jackknife replicates. Copy both the .phy and .model file into a new working dir, cd into the working dir, and run the following script (sample fixed proportion of number of genes):

	python jackknife_by_percent_genes.py <num_core> <jackknife proportion> DNA/aa

Alternatively, resampling by number of genes:

	python jackknife_by_number_genes.py DIR <num_core> <no. genes to resample> DNA/aa

Mapping jackknife results to the best tree. Use GTRCAT instead of PROTCATWAG for dna

	cat *result* >JK10_trees
	raxml -f b -t <bestTree> -z JK10_trees -T 2 -m PROTCATWAG -n <output_name>

Making a consensus tree from jackknife output

	phyutility -con -t 0.001 -in JK10_trees -out JK10_consensus

Translate taxon codes to make the tree more readable:
	
	python taxon_name_subst.py <table> <treefile>

A test data set can be found in examples/homology_inference. cd in to the dir examples/example_seq_processing and type:

	test_cds.sh