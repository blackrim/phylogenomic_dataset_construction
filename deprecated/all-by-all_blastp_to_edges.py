"""
input: concatenated raw blastp results with
cat *.rawblastp >all.rawblastp

lastp output file columns (separated by tab):
0-qseqid 1-qlen 2-sseqid 3-slen 4-frames 5-pident 6-nident 7-length 
8-mismatch 9-gapopen 10-qstart 11-qend 12-sstart 13-send 14-evalue 15-bitscore

output:
- .edges file with the filtered all-by-all blast connections
	with query,hit on each line
"""

import os,sys

HIT_FRACTION_CUT = 0.7
#both percent query and hit coverage have to >= this

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: python all-by-all_blastp_to_edges.py rawblastp"
		sys.exit()
	
	print "Reading raw blast output"
	infile = open(sys.argv[1],"r")
	outfile = open(sys.argv[1]+".edges","w")
	count = 0
	last_query,last_hit = "",""
	
	for line in infile:
		if len(line) < 3: continue #skip empty lines
		spls = line.strip().split("\t")
		# spls
		query,hit = spls[0],spls[2]
		if query == hit: continue #ignore empty lines and self-hits
		qlen,qstart,qend = float(spls[1]),float(spls[10]),float(spls[11])
		slen,sstart,send = float(spls[3]),float(spls[12]),float(spls[13])
		if last_query != "": #not at the very beginning of the blastn output
			if query == last_query and hit == last_hit:
				#expand the query range and hit ranges
				qstart = min(qstart,last_qstart)
				qend   = max(qend,last_qend)
				sstart = min(sstart,last_sstart)
				send   = max(send,last_send)
			else:#summarize last query-hit pair
				perc_qrange = (last_qend - last_qstart + 1) / last_qlen
				perc_srange = (last_send - last_sstart + 1) / last_slen
				#print last_qend,last_qstart,perc_qrange,last_send,last_sstart,perc_srange
				if perc_qrange >= HIT_FRACTION_CUT and perc_srange >= HIT_FRACTION_CUT:
					#output info for the last query-hit pair
					outfile.write(last_query+"\t"+last_hit+"\n")
					count += 1
					if count % 100000 == 0:
						print str(count)+" edges added"
		last_query,last_qlen,last_qstart,last_qend = query,qlen,qstart,qend
		last_hit,last_slen,last_sstart,last_send = hit,slen,sstart,send

			

