"""
Takes in a DIR with fasta files in it
blastn and fix directions. If there is any taxonID in the GENOMES
use this one as the reference direction
otherwise use a randome seq as the reference direction

.rawblastn file columns:
0-qseqid 1-qlen 2-sseqid 3-slen 4-frames 5-pident 6-nident 7-length 
8-mismatch 9-gapopen 10-qstart 11-qend 12-sstart 13-send 14-evalue 15-bitscore

"""

import os,sys
import networkx as nx
from Bio import SeqIO

FASTA_FILE_ENDING = ".fa"
QRANGE_COVERAGE_FILETER = 0.25 #qlen/abs(qend-qstart) has to be higher
GENOMES = ["Viti"]
SEQ_LEN_FILTER = 40

def get_name(label):
	return label.split("@")[0]
	
if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python dna_flip_mrna_direction_and_cut_ends.py fastaDIR num_cores >log"
		sys.exit(0)

	DIR = sys.argv[1]+"/"
	num_cores = sys.argv[2]
	files = os.listdir(DIR)
	for i in files:
		if i[-len(FASTA_FILE_ENDING):] != FASTA_FILE_ENDING or "_R" in i:
			continue
		outfile_name = i.replace(FASTA_FILE_ENDING,"_R"+FASTA_FILE_ENDING+".cut")
		if outfile_name in files:
			continue
		print i,outfile_name

		print "conducting blast on "+DIR+i
		cmd = "makeblastdb -in "+DIR+i+" -parse_seqids -dbtype nucl -out "+DIR+i+".db"
		print cmd
		os.system(cmd) #create blast database
		cmd = "blastn -db "+DIR+i+".db -query "+DIR+i
		cmd += " -evalue 10 -num_threads "+num_cores+" -max_target_seqs 100 -out "+DIR+i+".rawblastn "
		cmd += "-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
		#cmd = "swipe -d "+DIR+i+".db -i "+DIR+i+" -a "+num_cores+" -p blastn -o "+DIR+i+".swipe -m 8 -e 10"
		print cmd
		os.system(cmd) #carry out blastn
		print "blast is done"
		os.system("rm "+DIR+i+".db.n*") #remove the blast database

		#Get ends with no blast hits at all and cut them off
		infile = open(DIR+i+".rawblastn","r")
		cutDICT = {} #key is seqid, value is a list [start,end,length]
		for line in infile:
			if len(line) < 3: continue #skip empty lines
			spls = line.strip().split("\t")
			query,hit = spls[0],spls[2]
			if query == hit:
				continue #ignore self hits
			qlen,qstart,qend = int(spls[1]),int(spls[10]),int(spls[11])
			slen,sstart,send = int(spls[3]),int(spls[12]),int(spls[13])
					
			#get the widest range
			if query not in cutDICT:
				cutDICT[query] = [10000000,1,qlen] #[start,end,qlen]
			if hit not in cutDICT:
				cutDICT[hit] = [10000000,1,slen] #[start,end,slen]
			cutDICT[query][0] = min(cutDICT[query][0],qstart,qend) #compare starts
			cutDICT[query][1] = max(cutDICT[query][1],qstart,qend) #compare ends
			cutDICT[hit][0] = min(cutDICT[hit][0],sstart,send) #compare starts
			cutDICT[hit][1] = max(cutDICT[hit][1],sstart,send) #compare ends
		infile.close()
			
		#output seqid, start and end for cutting, and seq length
		outfile = open(i+".cut-info","w")
		for seqid in cutDICT:
			cut = cutDICT[seqid] #[start,end,length]
			start,end,length = cut[0],cut[1],cut[2]
			if start != 1 or end != length:
				outfile.write(seqid+"\t"+str(start)+"\t"+str(end)+"\t"+str(length)+"\n")
		outfile.close()
		print "Range to cut written to",i+".cut-info"

		print "Filtering blast results and write .edges files"
		infile = open(DIR+i+".rawblastn","r")
		outfile = open(DIR+i+".edges","w")
		for line in infile:
			if len(line) > 3:
				spls = (line.strip()).split("\t")
				query,hit = spls[0],spls[2]
				qlen,qstart,qend = float(spls[1]),float(spls[10]),float(spls[11])
				slen,sstart,send = float(spls[3]),float(spls[12]),float(spls[13])
				if query != hit:
					#if max(abs(qend-qstart)/qlen,abs(send-sstart)/slen) > QRANGE_COVERAGE_FILETER:
					if abs(qend-qstart)/qlen>QRANGE_COVERAGE_FILETER or abs(send-sstart)/slen > QRANGE_COVERAGE_FILETER:
						if (qend-qstart)*(send-sstart) > 0: dirc = "+"
						else: dirc = "-"
						outfile.write(query+"\t"+hit+"\t"+dirc+"\n")
		infile.close()
		outfile.close()
		
		print "Reading the graph from",DIR+i+".edges"
		infile = open(DIR+i+".edges","r")
		G = nx.Graph()
		start = None #find a model seq to start with. Use the last one
		#if no model seq is in the cluster
		for line in infile:
			if len(line) < 3: continue
			spls = line.strip().split("\t")
			query,hit,dirc = spls[0],spls[1],spls[2]
			if start == None and get_name(query) in GENOMES:
				start = query #set the start for traversal
			G.add_edge(query,hit,direction=dirc)
		if start == None: start = query
		infile.close()
		print "Starting traversal from",start

		#Breadth first traversal and label the direction
		edges = nx.bfs_edges(G,start)
		first = True #mark the start
		for edge in edges:
			node1,node2,dirc = edge[0],edge[1],G[edge[0]][edge[1]]["direction"]
			#print node1,node2,dirc
			if first == True: #set the start point
				G.node[node1]["keep"] = True #keep the original direction
				first = False #marked to flip the direction
			if G.node[node1]["keep"]:
				if dirc == "+": G.node[node2]["keep"] = True
				else: G.node[node2]["keep"] = False 
			else:
				if dirc == "+": G.node[node2]["keep"] = False
				else: G.node[node2]["keep"] = True
		print "Direction information for",len(G.node),"sequences recorded"
		
		#Iterate over all edges to look for conflicts
		conflict_present = False
		for edge in G.edges():
			node1,node2,dirc = edge[0],edge[1],G[edge[0]][edge[1]]["direction"]
			keep1,keep2 = G.node[node1]["keep"],G.node[node2]["keep"]
			if keep1 == keep2 and dirc == "+": continue #no conflict
			if keep1 != keep2 and dirc == "-": continue #no conflict
			conflict_present = True
			break
		if conflict_present:
			print "Conflict found in file "+i
			print edge[0],edge[1],G[edge[0]][edge[1]]["direction"],keep1,keep2
		else:
			print len(G.edges()),"edges checked. No conflict found."
		
		#print the updated fasta file with direction fixed
		handle = open(DIR+i,"r")
		outfile1 = open(DIR+i.replace(FASTA_FILE_ENDING,"_R"+FASTA_FILE_ENDING),"w")
		outfile2 = open(outfile_name, "w")
		for seq_record in SeqIO.parse(handle,"fasta"):
			seqid,seq = str(seq_record.id),str(seq_record.seq)
			try:
				cut = cutDICT[seqid] #[start,end,length]
			except: continue
			start,end = int(cut[0]),int(cut[1])
			if end-start > SEQ_LEN_FILTER:
				cutseq = seq[start-1:end]
				try:
					if G.node[seqid]["keep"]:
						outfile1.write(">"+seqid+"\n"+str(seq)+"\n")
						outfile2.write(">"+seqid+"\n"+str(cutseq)+"\n")
					else:
						outfile1.write(">"+seqid+"_R\n"+str(seq.reverse_complement())+"\n")
						outfile2.write(">"+seqid+"_R\n"+str(cutseq.reverse_complement())+"\n")
				except:
					print seqid,"had no direction information available after blastn filters"
		handle.close()
		outfile1.close()
		outfile2.close()


